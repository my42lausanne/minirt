/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   render_camera_ray.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: davifah <dfarhi@student.42lausanne.ch      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/08/31 19:14:53 by davifah           #+#    #+#             */
/*   Updated: 2022/10/20 14:57:47 by dfarhi           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "render.h"
#include "minirt.h"
#include "debug.h"
#include "minirt_math.h"
#include <math.h>

static void	setup_x_y_pos(
		const t_render_data *render, double *x_pos, double *y_pos)
{
	if (render->res_width % 2 != 0)
	{
		if (*x_pos > 0)
			*x_pos = ceil(*x_pos);
		else
			*x_pos = floor(*x_pos);
	}
	if (render->res_height % 2 != 0)
	{
		if (*y_pos > 0)
			*y_pos = ceil(*y_pos);
		else
			*y_pos = floor(*y_pos);
	}
}

t_vector	render_get_camera_direction(
		const t_vector v, const t_render_data *render, int x, int y)
{
	t_vector	v2;
	double		x_pos;
	double		y_pos;

	x_pos = x - (double)(render->res_width - 1) / 2.0f;
	y_pos = (render->res_height - 1 - y)
		- (double)(render->res_height - 1) / 2.0f;
	setup_x_y_pos(render, &x_pos, &y_pos);
	v2 = v;
	if (v.z < 0)
		y_pos = -y_pos;
	vector_rotate_x(&v2, -y_pos * render->aspp);
	vector_rotate_y(&v2, x_pos * render->aspp);
	if (DEBUG_SHIFTED_VECTOR)
		printf("%dx%d - shifted\n(%f,%f,%f) - pos x %.1lf; y %.1lf\n",
			x, y, v2.x, v2.y, v2.z, x_pos, y_pos);
	return (v_normalize(&v2));
}
