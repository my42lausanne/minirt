/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   debug.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mreymond <mreymond@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/08/30 18:26:52 by davifah           #+#    #+#             */
/*   Updated: 2022/11/09 10:21:39 by dfarhi           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef DEBUG_H
# define DEBUG_H

# define DEBUG_SHIFTED_VECTOR	0
# define DEBUG_PRINT_SETUP		0
# define DEBUG_LIGHT_OFF		0
# define DEBUG_INFINITE_CY		0
# define DBG_PRINT_RENDER_TIME	1

#endif
